# Command Line Commands

## Initialization

```Shell
npm install
```

Installs the dependencies.

## Development

```Shell
npm run dev
```

Starts the development server running on `http://localhost:8010`

## Generators

```Shell
npm run generate
```

Allows you to auto-generate boilerplate code for common parts of your
application, specifically `component`s, and `container`s.

## Building

```Shell
npm run build
```

Preps your app for deployment (does not run tests). Optimizes and minifies all files, piping them to the `dist` folder.

Upload the contents of `dist` to your web server to
see your work live!

## Unit testing

```Shell
npm run test
```

Tests your application with the unit tests specified in the `**/tests/*.js` files
throughout the application.

## Linting

```Shell
npm run lint
```

Lints your code and tries to fix any errors it finds.
