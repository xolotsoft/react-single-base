/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const path = require('path')
const merge = require('webpack-merge')
const common = require('./webpack.common')

module.exports = env =>
    merge(common(env), {
        mode: 'development',
        devtool: 'inline-source-map',
        devServer: {
            historyApiFallback: true
        },
        entry: {
            app: path.join(process.cwd(), 'src/app.jsx')
        },
        output: {
            path: path.join(process.cwd(), 'dist'),
            filename: 'js/bundle.js',
            publicPath: '/'
        }
    })
