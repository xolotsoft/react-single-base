const path = require('path')
const merge = require('webpack-merge')
const TerserPlugin = require('terser-webpack-plugin')
const common = require('./webpack.common')

const packageJsonFile = path.join(process.cwd(), 'package.json')
const packageJson = require(packageJsonFile)
const libraryName = packageJson.name
const outputFile = `${libraryName.replace('/', '-')}.js`

module.exports = merge(common, {
    entry: {
        app: path.join(process.cwd(), 'src/lib.js')
    },
    output: {
        filename: outputFile,
        library: libraryName,
        libraryTarget: 'amd',
        path: path.join(process.cwd(), 'lib')
    },
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()]
    }
})
