/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const path = require('path')
const merge = require('webpack-merge')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const safePostCssParser = require('postcss-safe-parser')

const common = require('./webpack.common')

module.exports = env =>
    merge(common(env), {
        mode: 'production',
        devtool: 'source-map',
        entry: {
            app: path.join(process.cwd(), 'src/app.jsx')
        },
        output: {
            path: path.join(process.cwd(), 'dist'),
            filename: 'js/[name].bundle.js',
            chunkFilename: 'js/[name].bundle.js',
            publicPath: '/'
        },
        optimization: {
            minimize: true,
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        parse: {
                            ecma: 8
                        },
                        compress: {
                            ecma: 5,
                            warnings: false,
                            comparisons: false,
                            inline: 2
                        },
                        mangle: {
                            safari10: true
                        },
                        output: {
                            ecma: 5,
                            comments: false,
                            ascii_only: true
                        }
                    },
                    sourceMap: true
                }),
                new OptimizeCSSAssetsPlugin({
                    cssProcessorOptions: {
                        parser: safePostCssParser,
                        map: {
                            inline: false,
                            annotation: true
                        }
                    },
                    cssProcessorPluginOptions: {
                        preset: [
                            'default',
                            { minifyFontValues: { removeQuotes: false } }
                        ]
                    }
                })
            ],
            splitChunks: {
                chunks: 'all',
                name: false
            },
            runtimeChunk: {
                name: entrypoint => `runtime-${entrypoint.name}`
            }
        }
    })
