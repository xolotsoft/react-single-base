module.exports = {
    description: 'This is a skeleton to component',
    prompts: [
        {
            type: 'input',
            name: 'name',
            message: 'Define the common component name',
            validate: value =>
                /.+/.test(value) ? true : 'The name is required'
        },
        {
            type: 'confirm',
            name: 'wantStateless',
            default: true,
            message: 'Do you want a Stateless component?'
        },
        {
            type: 'confirm',
            name: 'wantStyle',
            default: true,
            message: 'Do you want to add SCSS style sheet?'
        },
        {
            type: 'confirm',
            name: 'wantLoadable',
            default: true,
            message: 'Do you want to made your component loadable?'
        },
        {
            type: 'confirm',
            name: 'wantFlux',
            default: true,
            message: 'Do you want to add Flux architecture?'
        }
    ],
    actions: data => {
        const actions = [
            {
                type: 'add',
                path: '../src/components/{{dashCase name}}/index.jsx',
                templateFile: 'templates/components/index.jsx.hbs'
            }
        ]
        if (data.wantStateless) {
            actions.push({
                type: 'add',
                path: '../src/components/{{dashCase name}}/component.jsx',
                templateFile: 'templates/components/stateless.jsx.hbs'
            })
        } else {
            actions.push({
                type: 'add',
                path: '../src/components/{{dashCase name}}/component.jsx',
                templateFile: 'templates/components/class.jsx.hbs'
            })
        }
        if (data.wantStyle) {
            actions.push(
                {
                    type: 'add',
                    path: '../src/styles/sass/{{dashCase name}}.scss',
                    templateFile: 'templates/styles/container.scss.hbs'
                },
                {
                    type: 'append',
                    path: '../src/styles/sass/style.scss',
                    pattern: '/* STYLES IMPORTS */',
                    template: "@import '{{dashCase name}}';"
                }
            )
        }
        if (data.wantLoadable) {
            actions.push({
                type: 'add',
                path: '../src/components/{{dashCase name}}/loadable.jsx',
                templateFile: 'templates/components/loadable.jsx.hbs'
            })
        }
        if (data.wantFlux) {
            actions.push(
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/actions.js',
                    templateFile: 'templates/store/actions.js.hbs'
                },
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/constants.js',
                    templateFile: 'templates/store/constants.js.hbs'
                },
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/container.js',
                    templateFile: 'templates/store/container.js.hbs'
                },
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/index.js',
                    templateFile: 'templates/store/index.js.hbs'
                },
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/reducer.js',
                    templateFile: 'templates/store/reducer.js.hbs'
                },
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/saga.js',
                    templateFile: 'templates/store/saga.js.hbs'
                },
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/selectors.js',
                    templateFile: 'templates/store/selectors.js.hbs'
                },
                {
                    type: 'add',
                    path: '../src/store/{{dashCase name}}/state.js',
                    templateFile: 'templates/store/state.js.hbs'
                }
            )
        }
        return actions
    }
}
