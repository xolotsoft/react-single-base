module.exports = {
    description: 'This is a skeleton to common component',
    prompts: [
        {
            type: 'input',
            name: 'name',
            message: 'Define the common component name',
            validate: value =>
                /.+/.test(value) ? true : 'The name is required'
        },
        {
            type: 'confirm',
            name: 'wantStyle',
            default: false,
            message: 'Do you want to add SCSS style sheet?'
        }
    ],
    actions: data => {
        const actions = []
        if (data.wantStateless) {
            actions.push({
                type: 'add',
                path: '../src/components/common/{{dashCase name}}.jsx',
                templateFile: 'templates/components/stateless.jsx.hbs'
            })
        } else {
            actions.push({
                type: 'add',
                path: '../src/components/common/{{dashCase name}}.jsx',
                templateFile: 'templates/components/class.jsx.hbs'
            })
        }
        if (data.wantStyle) {
            actions.push(
                {
                    type: 'add',
                    path: '../src/styles/sass/{{dashCase name}}.scss',
                    templateFile: 'templates/styles/container.scss.hbs'
                },
                {
                    type: 'append',
                    path: '../src/styles/sass/style.scss',
                    pattern: '/* STYLES IMPORTS */',
                    template: "@import '{{dashCase name}}';"
                }
            )
        }
        return actions
    }
}
