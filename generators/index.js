const component = require('./component')
const common = require('./common')

module.exports = plop => {
    plop.setGenerator('Component', component)
    plop.setGenerator('Common Component', common)
}
