module.exports = {
    stories: ['../src/**/*.stories.(js|jsx)'],
    addons: [
        '@storybook/addon-actions',
        '@storybook/addon-links',
        '@storybook/addon-docs',
        '@storybook/addon-knobs/register',
        '@storybook/addon-viewport/register'
    ],
    webpackFinal: async config => {
        // do mutation to the config

        return config
    }
}
