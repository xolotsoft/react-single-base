module.exports = {
    // Automatically clear mock calls and instances between every test
    clearMocks: true,

    /**
     * An array of glob patterns indicating a set of files
     * for which coverage information should be collected
     */
    collectCoverageFrom: [
        'src/components/**/*.{js,jsx}',
        '!src/components/**/*.test.{js,jsx}',
        '!src/components/**/index.{js,jsx}',
        '!src/components/**/loadable.{js,jsx}',
        '!src/components/utils/**/*.{js,jsx}',
        '!src/components/app/**/*.{js,jsx}'
    ],

    // The directory where Jest should output its coverage files
    coverageDirectory: 'coverage',

    /**
     * A list of paths to modules that run some code to configure or set up the testing
     * framework before each test
     */
    setupFilesAfterEnv: ['<rootDir>/test.config.js'],

    // A list of paths to snapshot serializer modules Jest should use for snapshot testing
    snapshotSerializers: ['enzyme-to-json/serializer']
}
