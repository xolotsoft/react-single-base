/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import { connectRouter } from 'connected-react-router'
import { combineReducers } from 'redux'

/* INTERNAL IMPORTS */
import history from './history'

const staticReducers = {
    router: connectRouter(history)
}

const createReducer = asyncReducers =>
    combineReducers({ ...staticReducers, ...asyncReducers })

export default createReducer
