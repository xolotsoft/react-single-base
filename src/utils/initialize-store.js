/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'

/* INTERNAL IMPORTS */
import createReducer from './create-reducer'
import history from './history'

const sagaMiddleware = createSagaMiddleware({
    onError(error) {
        setImmediate(() => {
            throw error
        })
    }
})
const middlewares = [sagaMiddleware, routerMiddleware(history)]
const enhancers = [applyMiddleware(...middlewares)]

const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
        : compose

export const initializeStore = () => {
    const store = createStore(createReducer(), composeEnhancers(...enhancers))
    store.asyncReducers = {}
    store.asyncSagas = {}
    store.injectSaga = (key, asyncSaga) => {
        if (key in store.asyncSagas) return
        const task = sagaMiddleware.run(asyncSaga)
        store.asyncSagas[key] = task
    }
    store.injectReducer = (key, asyncReducer) => {
        store.asyncReducers[key] = asyncReducer
        store.replaceReducer(
            createReducer(store.asyncReducers),
            composeEnhancers(...enhancers)
        )
        return store
    }

    return store
}
