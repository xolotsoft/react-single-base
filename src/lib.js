/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import React from 'react'
import ReactDOM from 'react-dom'
import singleSpaReact from 'single-spa-react'

/* INTERNAL IMPORTS */
import rootComponent from './root'

const reactLifecycles = singleSpaReact({
    React,
    ReactDOM,
    rootComponent
})

export const { bootstrap } = reactLifecycles
export const { mount } = reactLifecycles
export const { unmount } = reactLifecycles
