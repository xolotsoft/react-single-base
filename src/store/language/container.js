/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'

/* INTERNAL IMPORTS */
import InjectReducer from '../../components/utils/inject-reducer'
import InjectSaga from '../../components/utils/inject-saga'
import { languageBindActions } from './actions'
import * as selectors from './selectors'
import { languageReducer } from './reducer'
import { languageSaga } from './saga'
import { LanguageSelector, LanguageProvider } from '../../components/language'

/**
 * Map the Redux store’s state to properties.
 * @param {*} state
 */
const mapStateToProps = state => ({
    lang: selectors.languageLangSelector(state)
})

/**
 * Map the Redux store’s actions to properties.
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(languageBindActions, dispatch)
})

/* Props to deals with your Redux store */
const withConnect = connect(mapStateToProps, mapDispatchToProps)

/* The reducer is dynamically injected */
const withReducer = InjectReducer('language', languageReducer)

/* The saga is dynamically injected */
const withSaga = InjectSaga('language', languageSaga)

/* Connect the React component to a Redux store with the injectors */
export const ProviderContainer = compose(
    withReducer,
    withSaga,
    withConnect
)(LanguageProvider)

export const SelectorContainer = compose(
    withReducer,
    withSaga,
    withConnect
)(LanguageSelector)
