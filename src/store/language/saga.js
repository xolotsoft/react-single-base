/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { put, takeEvery } from 'redux-saga/effects'

import { languageSagaActions as actions } from './actions'
import * as constants from './constants'

function* handleLanguageLangHandler(action) {
    yield put(actions.handleLanguageLangStartedAction())
    try {
        const { payload } = action
        yield put(actions.handleLanguageLangReducer(payload))
        yield put(actions.handleLanguageLangFinishedAction())
    } catch (error) {
        yield put(actions.handleLanguageLangErrorAction(error))
    }
}
export function* languageSaga() {
    yield takeEvery(
        constants.HANDLE_LANGUAGE_LANG_ACTION,
        handleLanguageLangHandler
    )
}
