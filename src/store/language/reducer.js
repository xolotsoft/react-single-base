/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import produce from 'immer'
import { handleActions } from 'redux-actions'

/* INTERNAL IMPORTS */
import initialState from './state'
import * as constants from './constants'

/**
 * The next functions specify how the application's state changes
 * in response to actions sent to the store.
 */

const handleLanguageLang = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.lang = payload
    })
}

const languageReducer = produce(
    handleActions(
        {
            [constants.HANDLE_LANGUAGE_LANG_REDUCER]: handleLanguageLang
        },
        initialState
    )
)

export { languageReducer }
