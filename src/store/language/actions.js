/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* INTERNAL IMPORTS */
import * as constants from './constants'

const handleLanguageLangReducer = payload => ({
    type: constants.HANDLE_LANGUAGE_LANG_REDUCER,
    payload
})

const handleLanguageLangAction = payload => ({
    type: constants.HANDLE_LANGUAGE_LANG_ACTION,
    payload
})
/**
 * this action mark the beginning of the Main click saga
 */
const handleLanguageLangStartedAction = () => ({
    type: constants.HANDLE_LANGUAGE_LANG_SAGA_STARTED
})
/**
 * this action mark the end of the Main click saga
 */
const handleLanguageLangFinishedAction = () => ({
    type: constants.HANDLE_LANGUAGE_LANG_SAGA_FINISHED
})
/**
 * this action is triggered if an error occurs in Main click saga.
 * @param {*} payload
 */
const handleLanguageLangErrorAction = payload => ({
    type: constants.HANDLE_LANGUAGE_LANG_SAGA_ERROR,
    payload
})

/* The next object of actions are accessible by the component */
export const languageBindActions = {
    handleLanguageLangAction
}
/* The next object of actions are accessible by the sagas */
export const languageSagaActions = {
    handleLanguageLangReducer,
    handleLanguageLangStartedAction,
    handleLanguageLangFinishedAction,
    handleLanguageLangErrorAction
}
