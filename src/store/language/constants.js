/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const HANDLE_LANGUAGE_LANG_REDUCER = 'language/handleLangReducer'
export const HANDLE_LANGUAGE_LANG_ACTION = 'language/handleLangAction'
export const HANDLE_LANGUAGE_LANG_SAGA_STARTED =
    'language/handleLangSagaStarted'
export const HANDLE_LANGUAGE_LANG_SAGA_FINISHED =
    'language/handleLangSagaFinished'
export const HANDLE_LANGUAGE_LANG_SAGA_ERROR = 'language/handleLangSagaError'
