/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import { connect } from 'react-redux'
import { compose } from 'redux'

/* INTERNAL IMPORTS */
import { App } from '../../components/app'

/**
 * Map the Redux store’s state to properties.
 * @param {*} state
 */
const mapStateToProps = () => ({})

/**
 * Map the Redux store’s actions to properties.
 * @param {*} dispatch
 */
const mapDispatchToProps = () => ({})

/* Props to deals with your Redux store */
const withConnect = connect(mapStateToProps, mapDispatchToProps)

/* Connect the React component to a Redux store with the injectors */
export const AppContainer = compose(withConnect)(App)
