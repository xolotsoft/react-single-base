/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import _ from 'lodash'

/**
 * Functions that accepts Redux state as an argument and returns
 * data that is derived from that state.
 */

export const mainValueSelector = state => _.get(state, ['main', 'value'])
export const mainTitleSelector = state => _.get(state, ['main', 'title'])
