/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import produce from 'immer'
import { handleActions } from 'redux-actions'

/* INTERNAL IMPORTS */
import initialState from './state'
import * as mainConstants from './constants'

/**
 * The next functions specify how the application's state changes
 * in response to actions sent to the store.
 */

const handleMainValue = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.value = payload
    })
}
const handleMainTitle = (state, action) => {
    const { payload } = action
    return produce(state, draft => {
        draft.title = payload
    })
}

const mainReducer = produce(
    handleActions(
        {
            [mainConstants.HANDLE_MAIN_VALUE_REDUCER]: handleMainValue,
            [mainConstants.HANDLE_MAIN_TITLE_REDUCER]: handleMainTitle
        },
        initialState
    )
)

export { mainReducer }
