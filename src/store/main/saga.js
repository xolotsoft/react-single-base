/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import {
    put,
    // takeLatest,
    takeEvery,
    // select,
    // call,
    delay
} from 'redux-saga/effects'

import { mainSagaActions as actions } from './actions'
import * as mainConstants from './constants'

function* handleMainClickHandler() {
    yield put(actions.handleMainClickStartedAction())
    try {
        yield put(actions.handleMainTitleAction('iniciando...'))
        yield delay(3000)
        yield put(actions.handleMainTitleAction('prueba ok'))
        yield delay(3000)
        yield put(actions.handleMainTitleAction('terminando...'))
        yield delay(3000)
        yield put(actions.handleMainTitleAction('terminado'))
        yield put(actions.handleMainClickFinishedAction())
    } catch (error) {
        yield put(actions.handleMainClickErrorAction(error))
    }
}
export function* mainSaga() {
    yield takeEvery(
        mainConstants.HANDLE_MAIN_CLICK_ACTION,
        handleMainClickHandler
    )
}
