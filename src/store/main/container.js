/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'

/* INTERNAL IMPORTS */
import InjectReducer from '../../components/utils/inject-reducer'
import InjectSaga from '../../components/utils/inject-saga'
import { mainBindActions } from './actions'
import * as selectors from './selectors'
import { mainReducer } from './reducer'
import { mainSaga } from './saga'
import { Main } from '../../components/main'

/**
 * Map the Redux store’s state to properties.
 * @param {*} state
 */
const mapStateToProps = state => ({
    value: selectors.mainValueSelector(state),
    title: selectors.mainTitleSelector(state)
})

/**
 * Map the Redux store’s actions to properties.
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(mainBindActions, dispatch)
})

/* Props to deals with your Redux store */
const withConnect = connect(mapStateToProps, mapDispatchToProps)

/* The reducer is dynamically injected */
const withReducer = InjectReducer('main', mainReducer)

/* The saga is dynamically injected */
const withSaga = InjectSaga('main', mainSaga)

/* Connect the React component to a Redux store with the injectors */
export const MainContainer = compose(withReducer, withSaga, withConnect)(Main)
