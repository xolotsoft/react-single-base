/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const HANDLE_MAIN_VALUE_REDUCER = 'main/handleValueReducer'
export const HANDLE_MAIN_TITLE_REDUCER = 'main/handleTitleReducer'
export const HANDLE_MAIN_CLICK_ACTION = 'main/handleClickAction'
export const HANDLE_MAIN_CLICK_SAGA_STARTED = 'main/handleClickSagaStarted'
export const HANDLE_MAIN_CLICK_SAGA_FINISHED = 'main/handleClickSagaFinished'
export const HANDLE_MAIN_CLICK_SAGA_ERROR = 'main/handleClickSagaError'
