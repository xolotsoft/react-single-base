/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* INTERNAL IMPORTS */
import * as mainConst from './constants'

/**
 * This action directly invokes the reducer to change the value
 * of the title in the state.
 * @param {*} payload
 */
const handleMainTitleAction = payload => ({
    type: mainConst.HANDLE_MAIN_TITLE_REDUCER,
    payload
})
/**
 * this action mark the beginning of the Main click saga
 */
const handleMainClickStartedAction = () => ({
    type: mainConst.HANDLE_MAIN_CLICK_SAGA_STARTED
})
/**
 * this action mark the end of the Main click saga
 */
const handleMainClickFinishedAction = () => ({
    type: mainConst.HANDLE_MAIN_CLICK_SAGA_FINISHED
})
/**
 * this action is triggered if an error occurs in Main click saga.
 * @param {*} payload
 */
const handleMainClickErrorAction = payload => ({
    type: mainConst.HANDLE_MAIN_CLICK_SAGA_ERROR,
    payload
})
/**
 * This action directly invokes the reducer to change the value
 * of the text input in the state.
 * @param {*} payload
 */
const handleMainInputAction = payload => ({
    type: mainConst.HANDLE_MAIN_VALUE_REDUCER,
    payload
})
/**
 * This action invokes a saga which starts a multi-step process
 * to manage the value of the title in the state.
 * @param {*} payload
 */
const handleMainClickAction = payload => ({
    type: mainConst.HANDLE_MAIN_CLICK_ACTION,
    payload
})
/* The next object of actions are accessible by the component */
export const mainBindActions = {
    handleMainInputAction,
    handleMainClickAction
}
/* The next object of actions are accessible by the sagas */
export const mainSagaActions = {
    handleMainTitleAction,
    handleMainClickStartedAction,
    handleMainClickFinishedAction,
    handleMainClickErrorAction
}
