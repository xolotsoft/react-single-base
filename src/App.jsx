/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import React from 'react'
import { render } from 'react-dom'

import 'regenerator-runtime/runtime'

/* INTERNAL IMPORTS */
import Root from './root'

const root = document.getElementById('main')

render(<Root />, root)
