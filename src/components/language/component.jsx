import React from 'react'
import p from 'prop-types'
import styled from 'styled-components'

const LanguageSelector = ({ className, actions, lang }) => {
    const handleChange = e => {
        actions.handleLanguageLangAction(e.target.value)
    }
    return (
        <select value={lang} onChange={handleChange} className={className}>
            <option value="en">En</option>
            <option value="es">Es</option>
        </select>
    )
}

LanguageSelector.propTypes = {
    actions: p.object,
    lang: p.string,
    className: p.string
}

LanguageSelector.defaultProps = {
    lang: 'en'
}

const LanguageSelectorStyled = styled(LanguageSelector)`
    width: 200px;
    background-color: var(--color-primary);
`

export { LanguageSelectorStyled as LanguageSelector }
