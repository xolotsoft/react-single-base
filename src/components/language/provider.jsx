import React from 'react'
import { IntlProvider } from 'react-intl'
import p from 'prop-types'

import messagesEn from '../../translations/en.json'
import messagesEs from '../../translations/es.json'

const LanguageProvider = ({ children, lang }) => {
    const messages = {
        es: messagesEs,
        en: messagesEn
    }
    return (
        <IntlProvider locale={lang} messages={messages[lang]}>
            {children}
        </IntlProvider>
    )
}

LanguageProvider.propTypes = {
    children: p.node,
    lang: p.string
}

LanguageProvider.defaultProps = {
    lang: 'en'
}

export { LanguageProvider }
