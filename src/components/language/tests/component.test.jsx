import React from 'react'
import { mount } from 'enzyme'
import { LanguageSelector } from '../component'

let wrapper

beforeEach(() => {
    wrapper = mount(<LanguageSelector />)
})

describe('Language Component', () => {
    it('Handle Select', () => {
        let lang = 'en'
        const actions = {
            handleLanguageLangAction: e => {
                lang = e
            }
        }
        wrapper.setProps({ actions, lang })
        wrapper.find('select').simulate('change', { target: { value: 'es' } })
        wrapper.setProps({ actions, lang })
        expect(wrapper.find('select').props().value).toBe('es')
    })
})
