import React from 'react'
import { mount } from 'enzyme'
import NotFound from '../not-found'

describe('Not Found Component', () => {
    it('render', () => {
        const wrapper = mount(<NotFound />)
        expect(wrapper.text()).toEqual('404 Not found')
    })
})
