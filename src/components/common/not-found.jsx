/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'

/**
 * Not found page
 */
const NotFound = () => <h1>404 Not found</h1>

export { NotFound }
