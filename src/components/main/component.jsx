/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import React from 'react'

/* INTERNAL IMPORTS */
import Molecule from './molecule'
import { SelectorContainer as LanguageSelector } from '../../store/language'

const Main = props => (
    <div className="main-container">
        <LanguageSelector />
        <Molecule name="Main" {...props} />
    </div>
)

export default Main
