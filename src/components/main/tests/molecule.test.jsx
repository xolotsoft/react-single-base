import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import 'regenerator-runtime/runtime'

import { ProviderContainer as LanguageProvider } from '../../../store/language'
import { initializeStore } from '../../../store'
import Molecule from '../molecule'

describe('Molecule Component', () => {
    it('Functions', () => {
        const onInputMock = jest.fn()
        const onButtonMock = jest.fn()
        const actions = {
            handleMainInputAction: onInputMock,
            handleMainClickAction: onButtonMock
        }
        const wrapper = mount(
            <Provider store={initializeStore()}>
                <LanguageProvider>
                    <Molecule actions={actions} />
                </LanguageProvider>
            </Provider>
        )
        wrapper.find('input').simulate('change', {
            target: { value: 'matched' }
        })
        expect(onInputMock.mock.calls[0][0]).toBe('matched')
        wrapper.find('button').simulate('click', { preventDefault() {} })
        expect(actions.handleMainClickAction).toHaveBeenCalled()
    })
})
