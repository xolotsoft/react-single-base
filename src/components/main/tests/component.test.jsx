import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import 'regenerator-runtime/runtime'

import { ProviderContainer as LanguageProvider } from '../../../store/language'
import { initializeStore } from '../../../store'
import Component from '../component'

describe('Main Component', () => {
    it('render', () => {
        const component = mount(
            <Provider store={initializeStore()}>
                <LanguageProvider>
                    <Component />
                </LanguageProvider>
            </Provider>
        )
        expect(component.find('.main-container')).toHaveLength(1)
    })
})
