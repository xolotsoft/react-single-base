/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import React from 'react'
import PropTypes from 'prop-types'

/* INTERNAL IMPORTS */
import Atom from './atom'

const Molecule = ({ title, name, value, actions }) => (
    <div className="main-molecule-container">
        <Atom name={name} />
        <input
            type="text"
            value={value}
            onChange={e => {
                actions.handleMainInputAction(e.target.value)
            }}
        />
        <p>
            <strong>Salida:</strong>
            {value}
        </p>
        <button type="button" onClick={() => actions.handleMainClickAction()}>
            SAGA
        </button>
        <p>
            <strong>Titulo:</strong>
            {title}
        </p>
    </div>
)

Molecule.propTypes = {
    name: PropTypes.string,
    value: PropTypes.string,
    title: PropTypes.string,
    actions: PropTypes.object
}

Molecule.defaultProps = {
    name: 'Main'
}
export default Molecule
