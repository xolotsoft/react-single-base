/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import React from 'react'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'

import icon from '../../assets/icon-react.svg'

const Img = styled.img`
    width: 160px;
    margin: 20px;
`
const Title = styled.p`
    font-size: 3em;
    margin: 10px 0;
`
const Atom = () => (
    <div>
        <Title>
            <FormattedMessage id="component.main.title.text" />
        </Title>
        <Img src={icon} alt="React" />
    </div>
)

export default Atom
