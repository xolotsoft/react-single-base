/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { ReactReduxContext } from 'react-redux'

/**
 * Dynamically injects a saga.
 *
 * @param {string} key A key saga
 * @param {function} saga Saga that will be injected
 */
const injectSaga = (key, saga) => WrappedComponent => {
    const Extended = props => {
        const context = React.useContext(ReactReduxContext)
        const { store } = context
        store.injectSaga(key, saga)
        return <WrappedComponent {...props} />
    }
    return Extended
}

export default injectSaga
