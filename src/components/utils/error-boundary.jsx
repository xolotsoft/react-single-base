/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import PropTypes from 'prop-types'

/**
 * Error boundaries are React components that catch JavaScript errors
 * anywhere in their child component tree.
 */
class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props)
        this.state = { hasError: false, error: false, info: '' }
    }

    static getDerivedStateFromError() {
        return { hasError: true }
    }

    componentDidCatch(error, info) {
        this.setState({ error, info })
    }

    render() {
        const btn = {
            width: '120px',
            height: '40px',
            border: 'none',
            margin: '0 10px'
        }
        const { hasError, error, info } = this.state
        const { children } = this.props
        if (hasError) {
            return (
                <div className="container" style={{ textAlign: 'center' }}>
                    <br />
                    <br />
                    <br />
                    <h5>Sucedió un error...</h5>
                    <p>
                        Lo sentimos, estamos teniendo dificultades técnicas
                        (como puedes ver)
                        <br />
                        prueba recargar la página, aveces funciona.
                    </p>
                    <button
                        type="button"
                        className="btn-default"
                        style={btn}
                        onClick={() => window.history.back()}
                    >
                        Atrás
                    </button>
                    <button
                        type="button"
                        className="btn-default"
                        style={btn}
                        onClick={() => window.location.reload(true)}
                    >
                        Recargar
                    </button>
                    <br />
                    <br />
                    <details
                        style={{ whiteSpace: 'pre-wrap', outline: 'none' }}
                    >
                        <p>{error && error.toString()}</p>
                        <p>{info.componentStack}</p>
                    </details>
                </div>
            )
        }
        return children
    }
}

ErrorBoundary.propTypes = {
    children: PropTypes.node.isRequired
}

export default ErrorBoundary
