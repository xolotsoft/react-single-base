/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react'
import { ReactReduxContext } from 'react-redux'

/**
 * Dynamically injects a reducer.
 *
 * @param {string} key A key reducer
 * @param {function} reducer Reducer that will be injected
 */
const injectReducer = (key, reducer) => WrappedComponent => {
    const Extended = props => {
        const context = React.useContext(ReactReduxContext)
        const { store } = context
        store.injectReducer(key, reducer)
        return <WrappedComponent {...props} />
    }
    return Extended
}

export default injectReducer
