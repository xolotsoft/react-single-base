import React from 'react'
import p from 'prop-types'

const preStyle = {
    position: 'relative',
    display: 'block',
    padding: '0.5em',
    marginTop: '0.5em',
    marginBottom: '0.5em',
    overflowX: 'auto',
    whiteSpace: 'pre-wrap',
    color: '#ffd1d1',
    fontFamily: 'Consolas, Menlo, monospace'
}

const Development = ({ error, info, line, column, file }) => (
    <div className="container" style={{ textAlign: 'left' }}>
        <p
            style={{
                ...preStyle,
                fontSize: '2em',
                fontWeight: 'bold',
                color: '#d26565'
            }}
        >
            An error has occurred
        </p>
        <pre
            style={{
                ...preStyle,
                backgroundColor: 'rgba(206, 17, 38, 0.7)',
                paddingBottom: '40px'
            }}
        >
            <h3 style={{ marginBottom: '0px' }}>Error</h3>
            <br />
            <code
                style={{
                    color: '#ffffff',
                    fontSize: '1.2em',
                    marginLeft: '30px'
                }}
            >
                {error.toString()}
            </code>
            <br />
            <p
                style={{
                    fontSize: '0.9em',
                    marginLeft: '60px',
                    marginBottom: '0px'
                }}
            >
                {`File: ${file}`}
            </p>
            <code style={{ marginLeft: '60px' }}>
                {`Ln:${line}, Col:${column} `}
            </code>
            {info && (
                <>
                    <h3 style={{ marginBottom: '0px' }}>Component Stack</h3>
                    <code>{info.componentStack}</code>
                </>
            )}
        </pre>
    </div>
)

Development.propTypes = {
    error: p.any,
    info: p.any,
    line: p.string,
    column: p.string,
    file: p.string
}

export { Development }
