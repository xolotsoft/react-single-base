/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import React from 'react'
import { Switch, Route } from 'react-router-dom'

/* INTERNAL IMPORTS */
import { MainContainer } from '../../store/main'
import { NotFound } from '../common/not-found'
import 'normalize.css'
import '../../styles/sass/style.scss'

const App = () => (
    <Switch>
        <Route path="/" exact component={MainContainer} />
        <Route component={NotFound} />
    </Switch>
)

export { App }
