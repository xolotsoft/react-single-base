/**
 * @copyright XolotSoft and its affiliates. All Rights Reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* EXTERNAL IMPORTS */
import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'

import 'regenerator-runtime/runtime'

/* INTERNAL IMPORTS */
import history from './utils/history'
import { App } from './components/app'
import { initializeStore } from './store'
import { ProviderContainer as LanguageProvider } from './store/language'
import ErrorBoundary from './components/utils/error-boundary'

export default () => (
    <ErrorBoundary>
        <Provider store={initializeStore()}>
            <LanguageProvider>
                <ConnectedRouter history={history}>
                    <App />
                </ConnectedRouter>
            </LanguageProvider>
        </Provider>
    </ErrorBoundary>
)
