import axios from 'axios'

class AxiosClient {
    constructor() {
        if (!AxiosClient.instance) {
            const instance = axios.create({})
            this.instance = instance
            AxiosClient.instance = this
        }
        return AxiosClient.instance
    }

    get(path, params, headers) {
        return this.instance.get(path, { params, headers })
    }

    post(path, params, headers) {
        return this.instance.post(path, params, { headers })
    }

    put(path, params, headers) {
        return this.instance.post(path, params, { headers })
    }

    delete(path, params, headers) {
        return this.instance.delete(path, { params, headers })
    }
}

const axiosClient = new AxiosClient()
Object.freeze(axiosClient)

export { axiosClient }
