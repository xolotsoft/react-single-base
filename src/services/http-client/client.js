import { axiosClient } from './axiosClient'

export class Client {
    /**
     *
     * @param {*} ClientInstance
     */
    constructor(ClientInstance = null) {
        if (ClientInstance) {
            this.service = new ClientInstance()
        } else {
            this.service = axiosClient
        }
    }

    /**
     *
     * @param {*} path
     * @param {*} optionals
     */
    get(path, { parameters, headers } = {}) {
        return this.service
            .get(path, parameters, headers)
            .then(response => Promise.resolve(response))
            .catch(error => Promise.reject(error))
    }

    /**
     *
     * @param {*} path
     * @param {*} parameters
     * @param {*} optionals
     */
    post(path, parameters, { headers } = {}) {
        return this.service
            .post(path, parameters, headers)
            .then(response => Promise.resolve(response))
            .catch(error => Promise.reject(error))
    }
}
